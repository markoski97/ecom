@extends('layout')

@section('title', $product->name)

@section('extra-css')

@endsection

@section('content')

    @component('components.breadcrumbs')
        <a href="/">Home</a>
        <i class="fa fa-chevron-right breadcrumb-separator"></i>
        <span><a href="{{ route('shop.index') }}">Shop</a></span>
        <i class="fa fa-chevron-right breadcrumb-separator"></i>
        <span>{{ $product->name }}</span>
    @endcomponent

    <div class="product-section container">

        <div>
            <div class="product-section-image">
                <img src="{{ productImage($product->image)}}" alt="product" id="currentImage" class="active">
            </div>

            <div class="product-section-images">

                <div class="product-section-thumbnail selected">
                    <img src="{{productImage($product->image)}}" alt="product">
                </div>

                @if($product->images)
                    @foreach(json_decode($product->images,true) as $image) {{--json decode oti images e vo samo string ( so json decod se prefrla vo array)--}}
                    <div class="product-section-thumbnail">
                        <img src="{{ productImage($image)}}" alt="product">
                    </div>
                    @endforeach
                @endif

            </div>

        </div>

        <div class="product-section-information">
            <h1 class="product-section-title">{{$product->name}}</h1>
            <div class="product-section-subtitle">{{$product->details}}</div>
            <div class="product-section-price">{{$product->asDollars($product->price)}}</div>

            <p>
                {!! $product->description !!}
            </p>

            {{--  <a href="{{route('cart.store')}}" class="button">Add to Cart</a>--}}

            <form action="{{route('cart.store')}}" method="POST">
                @csrf
                <input type="hidden" name="id" value="{{$product->id}}">
                <input type="hidden" name="name" value="{{$product->name}}">
                <input type="hidden" name="price" value="{{$product->price}}">
                <button type="submit" class="button button-plain">Add to Cart</button>
            </form>


        </div>
    </div> <!-- end product-section -->

    @include('partials.might-like')


@endsection

@section('extra-js')
    <script>
        (function () {
            const currentImage = document.querySelector('#currentImage');//zemija glavnata slika
            const images = document.querySelectorAll('.product-section-thumbnail');//zemigi site images
            images.forEach((element) => element.addEventListener('click', thumbnailClick));//na klik na images (nekoja od thumbnail)

            function thumbnailClick(e) {
                currentImage.classList.remove('active');//na pocetok ne e aktiv

                currentImage.addEventListener('transitionend', () => {
                    currentImage.src = this.querySelector('img').src; //zamenija glavnata slika so kliknatata
                    currentImage.classList.add('active');//dodajmu klasa aktiv ( za fadeinout ccs da go fati preku aktive)
                })


                images.forEach((element) => element.classList.remove('selected'));//selected izvvaja od site
                this.classList.add('selected');//dodaja selected samo na taja so e selektirana
            }
        })();
    </script>
@endsection
