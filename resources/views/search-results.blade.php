@extends('layout')

@section('title', 'Search Resoults')

@section('extra-css')

@endsection

@section('content')

    @component('components.breadcrumbs')
        <a href="/">Home</a>
        <i class="fa fa-chevron-right breadcrumb-separator"></i>
        <span>Search</span>
    @endcomponent

    <div class="search-container container">
        <h1>Search Resoults:</h1>
        <p>{{$products->count()}} result(s) for {{request()->input('query')}}</p>

        @foreach($products as $product)
            {{$product->name}}
        @endforeach

    </div> <!-- end search-section -->




@endsection


