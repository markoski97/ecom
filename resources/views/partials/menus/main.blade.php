<ul>
    {{--Ova e od dukumentacija od Voyager za da gi zema od menu bilder ( tools menu bilder )--}}
    @foreach($items as $menu_item)
        <li>
            <a href="{{$menu_item->link()}}">
                {{$menu_item->title}}

                @if($menu_item->title=='Cart')
                    @if(Cart::instance('default')->count()>0)
                        <span class="cart-count"><span>{{Cart::instance('default')->count()}}</span></span>
                    @endif
                @endif
            </a>
        </li>
    @endforeach
    {{--<li><a href="#">About</a></li>
    <li><a href="#">Blog</a></li>
    <li><a href="{{route('cart.index')}}">Cart <span class="cart-count"><span>3</span></span></a></li>--}}
</ul>
