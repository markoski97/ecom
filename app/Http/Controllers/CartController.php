<?php

namespace App\Http\Controllers;

use App\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Auth\Events\Validated;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CartController extends Controller
{

    public function index()
    {
        $mightAlsoLike=Product::MightAlsoLike()->get();
        return view('cart',compact('mightAlsoLike'));
    }

    public function store(Request $request)
    {
        //so ova fukcija proveruva dali produkto sho se Save vejce go ima vo kart
        $duplicates=Cart::search(function ($cartItem , $rowId) use ($request){
           return $cartItem->id === $request->id;
        });

        //Ako go ima daj mu poraka deka vejce go ima :D
        if($duplicates->isNotEmpty()){
            return redirect()->route('cart.index')->with('success_message','Item is allready in your cart');
        }

        Cart::add($request->id , $request->name ,1, $request->price)->associate(Product::class);

        return redirect()->route('cart.index')->with('success_message','Item was added to your cart');
    }

    public function emptyCart(){
        Cart::destroy();
    }
    public function emptySaved(){
        Cart::instance('saveForLate')->destroy();
    }

    public function destroy($id)
    {
        Cart::remove($id);

        return back()->with('success_message','Item has been removed');
    }

    //od kart da od vo save for later
    public function switchToSaveForLater($id){
        $item=Cart::get($id);

        Cart::remove($id);

        //so ova fukcija proveruva dali produkto sho se Save vejce go ima vo kart
        $duplicates= Cart::instance('saveForLate')->search(function ($cartItem , $rowId) use ($id){
            return $rowId === $id;
        });

        //Ako go ima daj mu poraka dekaf vejce go ima :D
        if($duplicates->isNotEmpty()){
            return redirect()->route('cart.index')->with('success_message','Item is allready saved for later');
        }

        Cart::instance('saveForLate')->add($item->id , $item->name ,1, $item->price)->associate(Product::class);

        return redirect()->route('cart.index')->with('success_message','Item has been Saved For Later');
    }

    public function update(Request $request, $id)
    {
        //ova go koristime za da namestinme validacija za quantaty da nemozi nekoj preku konzola da klaj prm 30
        $validator=Validator::make($request->all(),[
           'quantity'=>'required|numeric|between:1,15'
        ]);

        //ako validacijata padni daj poraka
        if($validator->fails()){
            session()->flash('errors', collect(['quantity must bee between 1 and 15']));
            return response()->json(['success' => false ]);
        }

        Cart::update($id , $request->quantity);

        session()->flash('success_message','Quantaty of the products is updated ');

        return response()->json(['success' => true ]);
    }
}
