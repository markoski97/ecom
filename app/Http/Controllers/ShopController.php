<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;

class ShopController extends Controller
{
    public function index()
    {
        if (request()->category) {
            $products = Product::with('categories')->whereHas('categories', function ($query) {
                $query->where('slug', request()->category);

            });
            $categories = Category::all();
            $categoryName =optional($categories->where('slug', request()->category))->first()->name;
        } else {
            $products = Product::where('featured',true);
            $categories = Category::all();
            $categoryName = 'Featured';
        }

        if (request()->sort == 'low_high') {
            $products = $products->orderBy('price')->paginate(9);
        } elseif (request()->sort == 'high_low') {
            $products = $products->orderBy('price','desc')->paginate(9);
        }
        else
        {
            $products=$products->paginate(9);
        }


        return view('shop', compact('products', 'categories', 'categoryName'));
    }

    public function show($slug)
    {
        $product = Product::where('slug', $slug)->firstOrFail();

        $mightAlsoLike = Product::where('slug', '!=', $slug)->MightAlsoLike()->get();

        return view('product ', compact('product', 'mightAlsoLike'));
    }

    public function search(Request $request){

        $query=$request->input('query');
        $products=Product::where('name','like',"%$query%")->get();
        return view('search-results',compact('products'));
    }
}
