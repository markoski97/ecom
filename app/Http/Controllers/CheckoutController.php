<?php

namespace App\Http\Controllers;

use App\Http\Requests\CheckoutRequest;
use App\Order;
use App\OrderProduct;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Cartalyst\Stripe\Laravel\Facades\Stripe;

class CheckoutController extends Controller
{

    public function index()
    {
        /*$tax=config('cart.tax') / 100; //vo config card se mesti taxo kolku da bidi (od Card paketo e ova)
        $discount=session()->get('coupon')['discount'] ?? 0;
        $newSubTotal=(Cart::subTotal() - $discount);
        $newTax=$newSubTotal * $tax;
        $newTotal= $newSubTotal * (1 + $tax);

        return view('checkout', compact('discount','newSubTotal','newTax','newTotal'));*/

        if (Cart::instance('default')->count() == 0) { //ako usero ili guesto nema nikakov produkt vo checkout redirect vo shop
            return redirect()->route('shop.index');
        }

        if(auth()->user() && request()->is('guestCheckout'))
        {
            return redirect(route('checkout.index'));
        }

        //da ne se povtoruvame getNumbers e private funkcija so vnatre variabli sho gi vrajcame kako collecija
        return view('checkout', with([
            'discount'=>$this->getNumbers()->get('discount'),
            'newSubTotal'=>$this->getNumbers()->get('newSubTotal'),
            'newTax'=>$this->getNumbers()->get('newTax'),
            'newTotal'=>$this->getNumbers()->get('newTotal'),
        ]));


    }

    public function store(CheckoutRequest $request)
    {

        $contents = Cart::content()->map(function ($item) {
            return $item->model->slug . ', ' . $item->qty;
        })->values()->toJson();

        try {
            $charge = Stripe::charges()->create([
                'amount' => $this->getNumbers()->get('newTotal') / 100,
                'currency' => 'CAD',
                'source' => $request->stripeToken,
                'description' => 'Order',
                'receipt_email' => $request->email,
                'metadata' => [
                    //change to Order ID after we start using DB
                    'contents' => $contents,
                    'quantity' => Cart::instance('default')->count(),
                    'discount' => collect(session()->get('coupon'))->toJson(),
                ],
            ]);

           $this->addToOrdersTables($request,null);

            // SUCCESSFUL
            Cart::instance('default')->destroy();
            session()->forget('coupon');

            return redirect()->route('confirmation.index')->with('success_message', 'Thank you! Your payment has been successfully accepted!');
        } catch (CardErrorException $e) {
            $this->addToOrdersTables($request,$e->getMessage());
            return back()->withErrors('Error! ' . $e->getMessage());
        }
    }


    protected function addToOrdersTables($request,$error){
        //inser into orders table
        $order=Order::create([
            'user_id'=>auth()->user() ? auth()->user()->id : null,
            'billing_email'=>$request->email,
            'billing_name'=>$request->name,
            'billing_address'=>$request->address,
            'billing_city'=>$request->city,
            'billing_province'=>$request->province,
            'billing_postalcode'=>$request->postalcode,
            'billing_phone'=>$request->phone,
            'billing_name_on_card'=>$request->name_on_card,
            'billing_discount'=>$this->getNumbers()->get('discount'),
            'billing_discount_code'=>$this->getNumbers()->get('code'),
            'billing_subtotal'=>$this->getNumbers()->get('newSubTotal'),
            'billing_tax'=>$this->getNumbers()->get('newTax'),
            'billing_total'=>$this->getNumbers()->get('newTotal'),
            'error'=>$error,
        ]);
        //insert into pivot table (order_product)
        foreach (Cart::content() as $item){
            OrderProduct::create([
                'order_id'=>$order->id,
                'product_id'=>$item->model->id,
                'quantity'=>$item->qty,
            ]);
        }
    }


    private function getNumbers(){
        $tax=config('cart.tax') / 100; //vo config card se mesti taxo kolku da bidi (od Card paketo e ova)
        $discount=session()->get('coupon')['discount'] ?? 0;
        $newSubTotal=(Cart::subTotal() - $discount);
        $newTax=$newSubTotal * $tax;
        $newTotal= $newSubTotal * (1 + $tax);
        $code=session()->get('coupon')['name'] ?? 0;

        return collect([
            'tax'=>$tax,
            'discount'=>$discount,
            'newSubTotal'=>$newSubTotal,
            'newTax'=>$newTax,
            'newTotal'=>$newTotal,
            'code'=>$code,
        ]);
    }
}
