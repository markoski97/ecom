<?php

namespace App\Http\Controllers;

use App\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;

class SaveForLaterController extends Controller
{
    public function destroy($id){
        Cart::instance('saveForLate')->remove($id);

        return back()->with('success_message','Item has been removed');
    }

    public function switchToCart($id)
    {
        $item=Cart::instance('saveForLate')->get($id);

        Cart::instance('saveForLate')->remove($id);

        //so ova fukcija proveruva dali produkto sho se Save vejce go ima vo kart
        $duplicates= Cart::instance('default')->search(function ($cartItem , $rowId) use ($id){
            return $rowId === $id;
        });

        //Ako go ima daj mu poraka deka vejce go ima :D
        if($duplicates->isNotEmpty()){
            return redirect()->route('cart.index')->with('success_message','Item is allready in your Cart');
        }

        Cart::instance('default')->add($item->id , $item->name ,1, $item->price)->associate(Product::class);

        return redirect()->route('cart.index')->with('success_message','Item has been moved to cart');
    }
}
