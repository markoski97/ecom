<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    //Funkcija za Pari ko che pokazuvash ubo da gi dava
    function asDollars($value) {
        if ($value<0) return "-".asDollars(-$value);
        return '$' . number_format($value, 2);
    }

    //funkcija za MightLike del kolku produkti da zema od baza
    function scopeMightAlsoLike($query){
        return $query->inRandomOrder()->take(4);
    }

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
}
