<?php

use Illuminate\Support\Facades\Route;


//front
Route::get('/','LandingPageController@index')->name('landing-page');

//Products etc Shop
Route::get('/shop','ShopController@index')->name('shop.index');
Route::get('/shop/{product}','ShopController@show')->name('shop.show');

Route::view('/product', 'product');

//Cart
Route::get('/cart','CartController@index')->name('cart.index');
Route::post('/cart','CartController@store')->name('cart.store');
Route::delete('/cart/{product}','CartController@destroy')->name('cart.destroy');
Route::patch('/cart/{product}','CartController@update')->name('cart.update');

//ovie dve se korista za da odi od Card vo save for later
Route::post('/cart/switchToSaveForLater/{product}','CartController@switchToSaveForLater')->name('cart.switchToSaveForLater');

//Ovie dve za od Save for later da odi vo kart
Route::delete('/saveForLater/{product}','SaveForLaterController@destroy')->name('saveForLater.destroy');
Route::post('/saveForLater/switchToSaveForLater/{product}','SaveForLaterController@switchToCart')->name('saveForLater.switchToCart');

//Deletiraj se sho ima vo card ili saved for later
Route::get('/cart_empty','CartController@emptyCart')->name('cart.empty');
Route::get('/saved_empty','CartController@emptySaved')->name('cart.emptySaved');

//Coupns
Route::post('/coupon','CouponsController@store')->name('coupon.store');
Route::delete('/coupon','CouponsController@destroy')->name('coupon.destroy');

//Checkout
Route::get('/checkout','CheckoutController@index')->name('checkout.index')->middleware('auth');
Route::post('/checkout','CheckoutController@store')->name('checkout.store');

//GuestCheckout
Route::get('/guestCheckout','CheckoutController@index')->name('guestCheckout.index');


Route::get('/thankyou','ConfirmationController@index')->name('confirmation.index');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/search', 'ShopController@search')->name('search');


